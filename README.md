# Singing Plants
## A Digital Biology Project

Welcome to our Repository!

Here we document the development of our "Singing Plants" project. We will host a Singing Plant [workshop at re:publica 2019](https://19.re-publica.com/en/session/give-plants-voice-digitally-translating-natures-needs) where you can learn about electronics and plants as well as solder your own PCB sensor.

<p align="center">
<img height="300" src="https://gitlab.com/alexander.kutschera/singingplants/raw/master/img/back_soldered.jpg"/> <img height="300" src="https://gitlab.com/alexander.kutschera/singingplants/raw/master/img/front_soldered.jpg"/>
</p>

For now this is mostly a collection of resources but we will fill this space with more information at a later point <3

- [Headphone jack pin out](http://www.circuitbasics.com/how-to-hack-a-headphone-jack/)
- [Audio Bootloader](https://github.com/vektorious/TinyAudioBoot)
- [Audio IDE integration](https://github.com/8BitMixtape/8Bit-Mixtape-NEO/wiki/4_3-IDE-integration)
- [Micro Coconut NEO schematics](https://github.com/8BitMixtape/8Bit-Mixtape-NEO/wiki/2_2-Micro-Coconut-NEO)
- [Gär Lämpli Hacktaria page](http://wlu18www30.webland.ch/wiki/G%C3%A4r_L%C3%A4mpli)

## Acknowledgements
This projects uses a lot of the bits and pieces already developed and used by the [8Bit-Mixtape-NEO](https://github.com/8BitMixtape/8Bit-Mixtape-NEO) project and especially the [TinyAudioBoot](https://github.com/ChrisMicro/AttinySound) developed by [ChrisMicro](https://github.com/ChrisMicro) (RoboterClub Freiburg), [Budi Prakosa](https://github.com/badgeek/) and others. The PCB design was done with the help of the great Inkscape plugin [svg2shenzhen](https://github.com/badgeek/svg2shenzhen).

## Instructions

### Flash Bootloader on ATtiny85

If you want to build your Singing Plant sensor from scratch you will need to flash a bootloader on the ATtiny85. We used the ATtiny85 ["Audio Bootloader"](https://github.com/ChrisMicro/AttinySound) which was developed by [ChrisMicro](https://github.com/ChrisMicro), [Budi Prakosa](https://github.com/badgeek/) and others which is an integral part of the great [8BitMixtape](https://github.com/8BitMixtape/8Bit-Mixtape-NEO) project. It is easily done using an ISP programmer and the command line utility [avrdude](https://www.nongnu.org/avrdude/) which should run on all platforms (yet we only tested it with MacOS so far). If you don't have an ISP programmer you can use an [Arduino](https://www.arduino.cc/en/Tutorial/ArduinoISP) and simply follow [this guide](https://www.instructables.com/id/Program-an-ATtiny-with-Arduino/) to step 4.

NOTE: If you got a kit with all the components from us we already flashed the bootloader on the supplied ATtiny85s

#### Set fuses
`avrdude -P *ISPdevice* -b 19200 -c avrisp -p t85 -U efuse:w:0xfe:m -U hfuse:w:0xdd:m -U lfuse:w:0xe1:m`

Instead of **ISPdevice** add the serial address of the Arduino (as ISP programmer) or your regular ISP programmer.

Example:
`avrdude -P /dev/tty.usbmodemFA131 -b 19200 -c avrisp -p t85 -U efuse:w:0xfe:m -U hfuse:w:0xdd:m -U lfuse:w:0xe1:m`

#### Burn bootloader
`avrdude -v -pattiny85 -c avrisp -P *ISPdevice* -b19200 -Uflash:w:*full/path/to/the/audio/bootloader.hex*:i`

Again, enter the serial address of your ISP programmer instead of **ISPdevice** and enter the **full path** to the audio boodloader .hex file.

Example:
`avrdude -v -pattiny85 -c avrisp -P /dev/tty.usbmodemFA131 -b19200 -Uflash:w:/Users/alexanderkutschera/GitLab/TinyAudioBoot/build/AudioBootAttiny_AudioPB3_PB1.hex:i`

### Create HEX/wav files

To program the sensor using the Arduino IDE you first have to integrate some things into the IDE itself. We are using the same infrastructure as the 8BitMixtabe-NEO board and that's why you can follow [these instructions](https://github.com/8BitMixtape/8Bit-Mixtape-NEO/wiki/4_3-IDE-integration).

NOTE: We used a different core for our scripts (because otherwise producing sounds with tone() does not work). Here are our settings:

In the Arduino IDE select:

- **Board:** 8BitMixtapeNeo
- **Bootloader:** AudioBoot Audio: PB3/PIN2 LED: PB1/PIN6
- **Core:** TeenyRiot core 16MHz (PLL/NO.BOD)
- **Port:** leave empty
