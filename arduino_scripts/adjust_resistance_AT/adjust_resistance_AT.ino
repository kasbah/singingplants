// this script combines sound, moisture and light test
#include <Adafruit_NeoPixel.h>
int plant_pin = 1;
int plant_read = 1;
int plant_value = 0;
int sound_out = 0;
int light_out = 4;

int i;

bool moisture = false;
bool light = false;

Adafruit_NeoPixel strip = Adafruit_NeoPixel(2, 4, NEO_GRB + NEO_KHZ800);

void setup() {
  // put your setup code here, to run once:
  pinMode(plant_pin, OUTPUT);
  //Serial.begin(9600);
  strip.begin();
  strip.setBrightness(30);
  strip.show();
}

void loop() {
  // put your main code here, to run repeatedly:
  digitalWrite(plant_pin, HIGH);
  delay(500);
  plant_value = analogRead(plant_read);
  plant_value = plant_value/10;

  for (i = 0; i <= plant_value; i++) {
    tone(sound_out,7000, 1000);
    delay(500);
    tone(sound_out,3000, 1000);
    delay(500);
  } 

  digitalWrite(plant_pin, LOW);
  delay(5000);
}
