// this script combines sound, moisture and light test
#include <Adafruit_NeoPixel.h>
int plant_pin = 1;
int plant_read = 1;
int plant_value = 0;
int sound_out = 0;
int light_out = 4;

bool moisture = false;
bool light = false;

Adafruit_NeoPixel strip = Adafruit_NeoPixel(2, 4, NEO_GRB + NEO_KHZ800);

void setup() {
  // put your setup code here, to run once:
  pinMode(plant_pin, OUTPUT);
  //Serial.begin(9600);
  strip.begin();
  strip.setBrightness(30);
  strip.show();
}

void loop() {
  // put your main code here, to run repeatedly:
  digitalWrite(plant_pin, HIGH);
  delay(500);
  plant_value = analogRead(plant_read);

  if (plant_value >= 150) {
    moisture = true;
    tone(sound_out,7000, 1000);
    delay(500);
    tone(sound_out,6000, 1000);
    delay(500);
    tone(sound_out,5000, 1000);
    delay(500);
    tone(sound_out,4000, 1000);
    delay(500);
    tone(sound_out,5000, 1000);
    delay(500);
    tone(sound_out,3000, 1000);
    delay(500);
    tone(sound_out,3500, 1000);
    delay(500);
  } else {
    moisture = false;
    tone(sound_out,3000, 1000);
  }

if (plant_value >= 150) {
  light = true;
  strip.setPixelColor(0, 255, 255, 255);
  strip.setPixelColor(1, 0, 255, 80);
  strip.show();
  }
else {
  light = false;
  strip.setPixelColor(0, 255, 0, 0);
  strip.setPixelColor(1, 0, 255, 0);
  strip.show();
  }
  
  //Serial.println(plant_value);
  //Serial.println(moisture);
  digitalWrite(plant_pin, LOW);
  delay(1000);
}
